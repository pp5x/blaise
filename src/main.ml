(*
 ** main.ml for Blaise
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Dec 29 17:48:26 2012 Pierre Pagnoux
** Last update Thu Jan  3 00:04:40 2013 Pierre Pagnoux
 *)
type conf = {
  mutable eval : bool;
}

let main () =
  let c = { eval = false; } in
  Arg.parse [("-eval", Arg.Unit (fun () -> c.eval <- true), "Evaluation of the file.")]
    (fun _ -> ())
    "./blaise FILE [OPTION]";
  if Array.length Sys.argv < 2 then
    invalid_arg "You must specify a file."
  else
    begin
      if c.eval then
        let file = open_in Sys.argv.(2) in
        let ast = Parser.prg Lexer.token (Lexing.from_channel file) in
        ignore (Eval.prg ast)
      else
        let file = open_in Sys.argv.(1) in
        let ast = Parser.prg Lexer.token (Lexing.from_channel file) in
        Print.print_prg ast
    end;
  exit 0

let _ = main ()

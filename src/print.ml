(*
 ** print.ml for Blaise
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sun Dec 30 16:03:15 2012 Pierre Pagnoux
** Last update Sun Dec 30 22:55:12 2012 Pierre Pagnoux
 *)

module F = Format
module A = Ast

let rec print_expr = function
  | A.Int i -> F.printf "%i" i
  | A.Var s -> F.printf "%s" s
  | A.BinOp (a, s, b) ->
    print_expr a;
    F.printf " %s " s;
    print_expr b;
  | A.UniOp (s, a) ->
    F.printf "%s " s;
    print_expr a;
  | A.Call (s, l) ->
    F.printf "%s(" s;
    let rec iter = function
      | [] -> ()
      | a::(_::_ as l) ->
        print_expr a;
        F.printf ", ";
        iter l
      | a::l ->
        print_expr a;
        iter l
    in
    iter l;
    F.printf ")"

let rec print_statement statement =
  let rec iter_statl = function
    | [] -> ()
    | a::(_::_ as l) ->
      print_statement a;
      F.printf "@ ";
      iter_statl l
    | a::l ->
      print_statement a;
      iter_statl l
  in
  match statement with
    | A.Assign (s, expr) ->
      F.printf "@[%s =@ " s;
      print_expr expr;
      F.printf ";@]"
    | A.Expr e ->
      F.printf "@[";
      print_expr e;
      F.printf ";@]"
    | A.If (expr, th, el) ->
      F.printf "@[<v 4>if ( ";
      print_expr expr;
      F.printf " ) {@ ";
      iter_statl th;
      if el <> [] then
        begin
          F.printf "@;<1 -4>} else {@ ";
          iter_statl el;
        end;
      F.printf "@;<1 -4>}@]"
    | A.While (expr, l) ->
      F.printf "@[<v 4>while ( ";
      print_expr expr;
      F.printf " ) {@ ";
      iter_statl l;
      F.printf "@;<1 -4>}@]"
    | A.Return expr ->
      F.printf "@[return ";
      print_expr expr;
      F.printf ";@]"

let rec iter_statl = function
  | [] -> ()
  | a::(_::_ as l) ->
    print_statement a;
    F.printf "@ ";
    iter_statl l
  | a::l ->
    print_statement a;
    iter_statl l

let print_funcdecl f =
  let rec iter_stringl = function
    | [] -> ()
    | a::(_::_ as l) ->
      F.printf "%s, " a;
      iter_stringl l
    | a::l ->
      F.printf "%s" a;
      iter_stringl l
  in
  F.printf "@[<v 4>%s(" f.A.fname;
  iter_stringl f.A.fparams;
  F.printf ") {@ ";
  if f.A.fvars <> [] then
    begin
      F.printf "@[<h>vars ";
      iter_stringl f.A.fvars;
      F.printf ";@]@ "
    end;
  iter_statl f.A.fbody;
  F.printf "@;<1 -4>}@;<1 -4>@]"

let print_entry e =
  let f =
    {
      A.fname = "main";
      A.fbody = e.A.mainbody;
      A.fparams = [];
      A.fvars = e.A.mainvars;
    }
  in
  print_funcdecl f

let print_prg p =
  List.iter print_funcdecl p.A.func;
  print_entry p.A.main

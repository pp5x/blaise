type expr =
  | Int of int
  | Var of string
  | BinOp of expr * string * expr
  | UniOp of string * expr
  | Call of string * expr list

type statement =
  | Assign of string * expr
  | Expr of expr
  | If of expr * statement list * statement list
  | While of expr * statement list
  | Return of expr

type funcdecl =
    {
      fname : string;
      fbody : statement list;
      fparams : string list;
      fvars : string list;
    }

type entry =
    {
      mainvars : string list;
      mainbody : statement list;
    }

type prg =
    {
      func : funcdecl list;
      main : entry;
    }

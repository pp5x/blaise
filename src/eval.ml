(*
 ** eval.ml for Blaise
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sun Dec 30 23:36:31 2012 Pierre Pagnoux
** Last update Thu Jan  3 00:02:29 2013 Pierre Pagnoux
 *)

module A = Ast
module M = Hashtbl

exception Return of int

type env =
    {
      func : A.funcdecl list;
      mutable mem  : (string, int) M.t;
    }

let int_of_bool = function
  | true -> 1
  | false -> 0

let bool_of_int = function
  | 0 -> false
  | _ -> true

let rec expr env = function
  | A.Int i -> i
  | A.Var s -> M.find env.mem s
  | A.BinOp (x, s, y) ->
    let a = expr env x in
    let b = expr env y in
    let op a b = function
      | "+" -> a + b
      | "-" -> a - b
      | "*" -> a * b
      | "/" -> a / b
      | ">" -> int_of_bool (a > b)
      | "<" -> int_of_bool (a < b)
      | ">=" -> int_of_bool (a >= b)
      | "<=" -> int_of_bool (a <= b)
      | "==" -> int_of_bool (a = b)
      | "||" -> int_of_bool (bool_of_int a || bool_of_int b)
      | "&&" -> int_of_bool (bool_of_int a && bool_of_int b)
      | _ -> failwith "BinOp not implemented"
    in
    op a b s
  | A.UniOp (s, x) ->
    let a = expr env x in
    let op a = function
      | "!" -> int_of_bool (not (bool_of_int a))
      | _ -> failwith "UniOp not implemented."
    in
    op a s
  | A.Call (s, l) ->
    match s with
      | "write" ->
        Printf.printf "%d\n" (expr env (List.hd l));
        1
      | name ->
        let f = (List.find (fun f -> name = f.A.fname) env.func) in
        let p = (List.map (expr env) l) in
        funcdecl env f p

and statement env = function
  | A.Assign (s, e) -> M.replace env.mem s (expr env e)
  | A.Expr e -> let _ = expr env e in ()
  | A.If (c, t, e) ->
    List.iter (statement env)
      (if bool_of_int (expr env c) then t else e)
  | A.While (c, l) ->
    if bool_of_int (expr env c) then
      begin
        List.iter (statement env) l;
        statement env (A.While (c, l))
      end
    else ()
  | A.Return e -> raise (Return (expr env e))

and funcdecl env f p =
  let backup = M.copy env.mem in
  M.reset env.mem;
  List.iter2 (M.add env.mem) f.A.fparams p;
  let decl_list = List.map (M.add  env.mem) f.A.fvars in
  let rec init_value = function
    | [] -> ()
    | f::l ->
      f 0;
      init_value l
  in
  init_value decl_list;
  try
    List.iter (statement env) f.A.fbody;
    env.mem <- backup;
    0
  with
    | Return n ->
      env.mem <- backup; n
    | _ -> failwith "Error during function execution"

let entry env f =
  let func = {
    A.fname = "main";
    A.fbody = f.A.mainbody;
    A.fparams = [];
    A.fvars = f.A.mainvars;
  }
  in
  funcdecl env func []

let prg p =
  let env =
    {
      func = p.A.func;
      mem = M.create 42;
    }
  in
  entry env p.A.main

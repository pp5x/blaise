##
## Makefile for Blaise
## 
## Made by Pierre Pagnoux
## <Pierre.Pagnoux@gmail.com>
## 
## Started on  Sat Dec 29 19:22:14 2012 Pierre Pagnoux
## Last update Sun Dec 30 23:16:54 2012 Pierre Pagnoux
##

SUBDIRS = src
EXEC = blaise

all : $(SUBDIRS)

clean : COMMAND = clean
clean : $(SUBDIRS)
	@rm -f $(EXEC) *#

$(SUBDIRS) : FORCE
	@$(MAKE) -C $@ $(COMMAND)

FORCE :

